INTRODUCTION
------------

This module extends EU Cookie Compliance module to be compliant with the Italian
law, which requires the preemptive blocking of cookies when the user has not
yet accepted the popup conditions.

It adopts a lightweight fully server-side approach to exclude Javascript files
blocks, and node fields, from any rendered page until the cookie policy has been accepted.

INSTALLATION
------------

1. Install and configure EU Cookie Compliance so that the popup is enabled
and viewable.
See https://www.drupal.org/project/eu_cookie_compliance.

2. Install IT Cookie Compliance.

3. Go to the admin/config/system/it_cookie_compliance page to set the
javascript, blocks and fields to be excluded before the acceptance. 

