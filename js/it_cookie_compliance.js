/**
 * @file
 * Handle automatic redirect after agreement.
 */

(function ($) {
  Drupal.behaviors.it_cookie_compliance = {
    attach: function (context, settings) {

      $(window).on('load', function () {
        $(".agree-button").click(
                function () {
                  window.location.href = document.URL;
                }
        );
      });
    }
  }

})(jQuery);

